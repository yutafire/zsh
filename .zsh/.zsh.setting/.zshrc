export PS1="%~ $ "
autoload -Uz promptinit
promptinit
prompt powerline
autoload -U compinit
compinit

export ENV="local"
export PATH="/usr/local/sbin:$PATH"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export PATH="/Users/yuta/Program/make_os/prog/tools/nasm-2.15.03/:$PATH"
export PATH="$HOME/tools/:$PATH"
eval "$(pyenv init -)"
export PATH="$PATH:$HOME/flutter/bin"

# alias
alias vi='nvim'
alias ll='ls -la'
export LSCOLORS='gxfxcxdxbxegedabagacad'
export PATH=/Users/yuta/.nodebrew/current/bin:/Users/yuta/.pyenv/shims:/Users/yuta/tools/:/Users/yuta/Program/make_os/prog/tools/nasm-2.15.03/:/Users/yuta/.pyenv/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/go/bin:/Library/Apple/usr/bin:/Users/yuta/.pyenv/shims:/Users/yuta/tools/:/Users/yuta/Program/make_os/prog/tools/nasm-2.15.03/:/Users/yuta/.pyenv/bin:/Users/yuta/flutter/bin:/Users/yuta/flutter/bin
